// ==UserScript==
// @name         arXiv rename
// @include      https://arxiv.org/*
// @version      1.0
// @description  Add a Download PDF option and rename the download pdf to '[xxxx.xxxx] Title (authors)' 
// @author       Stathis Vitouladitis
// @date         01.01.2021
// @namespace    https://gitlab.com/st-vit/arxiv-rename
// ==/UserScript==

// fork from EvanL00
var dow = function() {
    'use strict';
    // find the title and the authors
    var title = document.getElementsByClassName("title mathjax")[0].innerText;
    var authors = document.getElementsByClassName("authors")[0].innerText;
    //find where to put the tag
    var loc = document.getElementsByClassName("full-text")[0].getElementsByTagName('ul');
    var obj = document.createElement("li");
    //get the pdf url
    var pdfurl = document.getElementsByClassName("full-text")[0].getElementsByTagName('ul')[0].getElementsByTagName('a')[0].href;
    //check name
    if (!pdfurl.endsWith(".pdf")) {
     pdfurl = pdfurl + '.pdf';
    }
    //change name
    var fileName = document.title + ' (' + authors + ')' + '.pdf';
    obj.innerHTML = '<a download='+ '"'+ fileName + '"' + ' href=' + pdfurl +'>Download PDF</a>';
    loc[0].insertBefore(obj, loc[0].childNodes[0]);
};
dow();
